//
//  CalculatorBrain.h
//  Calculator
//
//  Created by red on 4/12/13.
//  Copyright (c) 2013 Kraftwerking, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

-(void)pushOperand:(double)operand;
-(double)performOperation:(NSString *)operation;

@end
