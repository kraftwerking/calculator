//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by red on 4/12/13.
//  Copyright (c) 2013 Kraftwerking, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
