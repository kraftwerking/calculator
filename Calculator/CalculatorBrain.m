//
//  CalculatorBrain.m
//  Calculator
//
//  Created by red on 4/12/13.
//  Copyright (c) 2013 Kraftwerking, LLC. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain()
@property(nonatomic,strong) NSMutableArray *operandStack;
@end

@implementation CalculatorBrain

@synthesize operandStack = _operandStack;

-(NSMutableArray *)operandStack
{
    if(_operandStack == nil) _operandStack = [[NSMutableArray alloc] init];
    return _operandStack;
}

-(void)setOperandStack:(NSMutableArray *)operandStack
{
    _operandStack = operandStack;
}

-(double)popOperand
{
    NSNumber *operandObject = [self.operandStack lastObject];
    if(operandObject) [self.operandStack removeLastObject];
    return [operandObject doubleValue];
}

-(void)pushOperand:(double)operand
{
    [self.operandStack addObject:[NSNumber numberWithDouble:operand]];
}

-(double)performOperation:(NSString *)operation
{
    double result = 0;
    //calculate result
    if([operation isEqualToString:@"+"]) {
        result = [self popOperand] + [self popOperand];
    } else if([operation isEqualToString:@"*"]) {
        result = [self popOperand] * [self popOperand];
    } else if([operation isEqualToString:@"/"]) {
        result = [self popOperand] / [self popOperand];
    } else if([operation isEqualToString:@"-"]) {
        result = [self popOperand] - [self popOperand];
    } else if([operation isEqualToString:@"sin"]) {
        result = sin([self popOperand]);
    } else if([operation isEqualToString:@"cos"]) {
        result = cos([self popOperand]);
    } else if([operation isEqualToString:@"sqrt"]) {
        result = sqrt([self popOperand]);
    }

    
    [self pushOperand: result];
    return result;
}
@end
