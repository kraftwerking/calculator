//
//  CalculatorViewController.m
//  Calculator
//
//  Created by red on 4/12/13.
//  Copyright (c) 2013 Kraftwerking, LLC. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic,strong) CalculatorBrain *brain;
@end

@implementation CalculatorViewController

@synthesize display = _display;
@synthesize displayOperation = _displayOperation;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
@synthesize brain = _brain;

-(CalculatorBrain *)brain
{
    if(!_brain) _brain = [[CalculatorBrain alloc]init];
    return _brain;
}

- (IBAction) digitPressed:(UIButton *)sender
{
    NSString *digit = sender.currentTitle;
    
    //if pi
    if([digit isEqualToString:@"π"]) {
        NSString *piString = [NSString stringWithFormat: @"%f", M_PI];
        digit = piString;
    }
    
    NSLog(@"digit pressed = %@", digit);
    NSRange isNumberDecimal = [self.display.text rangeOfString:@"."];
    if(self.userIsInTheMiddleOfEnteringANumber){ 
        if([digit isEqualToString:@"."]) {
            if (isNumberDecimal.location == NSNotFound) {
                self.display.text = [self.display.text stringByAppendingString:digit];
                self.displayOperation.text = [self.displayOperation.text stringByAppendingString:digit];

            }
        }else{ //user did not press . button
            self.display.text=[self.display.text stringByAppendingString:digit];
            self.displayOperation.text = [self.displayOperation.text stringByAppendingString:digit];
        }

    } else {
        self.display.text = digit;
        self.displayOperation.text = [self.displayOperation.text stringByAppendingString:digit];
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
}

- (IBAction)enterPressed {
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInTheMiddleOfEnteringANumber = NO;
}

- (IBAction)operationPressed:(UIButton *)sender {
    if(self.userIsInTheMiddleOfEnteringANumber)[self enterPressed];
    double result = [self.brain performOperation:sender.currentTitle];
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.display.text = resultString;
    NSString *oper = sender.currentTitle;

    self.displayOperation.text = [self.displayOperation.text stringByAppendingString:oper];

}

@end
