//
//  CalculatorViewController.h
//  Calculator
//
//  Created by red on 4/12/13.
//  Copyright (c) 2013 Kraftwerking, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UILabel *displayOperation;
@end
